﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyBezierTest : MonoBehaviour {

	// Use this for initialization
    public Transform startPos;
    Vector3 centerPos;
    public Transform endPos;
    public Transform moveObj;
    public Transform midObj;
    public float speed;

    float time;
    float t = 0;
    float deltaTime;
    float timeCount;
    Vector3 lastPos;
    Vector3 nowPos;
	void Start () {
        time = Vector3.Distance(startPos.localPosition, endPos.localPosition) / speed;
        centerPos = new Vector3(startPos.localPosition.x + (endPos.localPosition.x - startPos.localPosition.x) * 0.5f, Mathf.Abs((endPos.localPosition.y + startPos.localPosition.y) / 2f + (endPos.localPosition.x - startPos.localPosition.x) / 1.5f), 0);
        midObj.localPosition = centerPos;
        deltaTime = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (t < 1)
        {
            t = Mathf.Lerp(0, 1, deltaTime);
            nowPos = Mathf.Pow((1f - t), 2) * startPos.localPosition + 2 * t * (1f - t) * centerPos + Mathf.Pow(t, 2) * endPos.localPosition;
            if (lastPos != Vector3.zero)
            {
                moveObj.rotation = CalAngle(Vector3.right, nowPos, lastPos);
                moveObj.gameObject.SetActive(true);
            }
                
            timeCount += Time.deltaTime;
            moveObj.localPosition = nowPos;
            lastPos = nowPos;
        }
        deltaTime += Time.deltaTime / time;
	}

    Quaternion CalAngle(Vector3 baseDirection, Vector3 toPos, Vector3 fromPos)
    {
        var direction = toPos - fromPos;
        var angle = Vector3.Angle(baseDirection, direction);
        angle = toPos.y > fromPos.y ? angle : -angle;
        return Quaternion.Euler(0, 0, angle);
        //transform.rotation = Quaternion.Euler(0, 0, angle);
    }
}
